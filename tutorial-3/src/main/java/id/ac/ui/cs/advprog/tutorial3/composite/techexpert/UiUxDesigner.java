package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class UiUxDesigner extends Employees {
    // TODO Implement
    public static final double MIN_SALARY = 90000.00;

    public UiUxDesigner(String name, double salary) {
        // TODO Implement
        this.name = name;
        this.salary = salary;
        this.role = "UI/UX Designer";
        if (salary < MIN_SALARY) {
            throw new IllegalArgumentException("Salary must not below " + MIN_SALARY);
        }
    }

    @Override
    public double getSalary() {
        // TODO Implement
        return this.salary;
    }
}
