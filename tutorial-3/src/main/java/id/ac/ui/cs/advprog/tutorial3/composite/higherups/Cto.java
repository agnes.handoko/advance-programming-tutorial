package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Cto extends Employees {
    public static final double MIN_SALARY = 100000.00;

    public Cto(String name, double salary) {
        // TODO Implement
        this.name = name;
        this.salary = salary;
        this.role = "CTO";
        if (salary < MIN_SALARY) {
            throw new IllegalArgumentException("Salary must not below " + MIN_SALARY);
        }
    }

    @Override
    public double getSalary() {
        // TODO Implement
        return this.salary;
    }
}
