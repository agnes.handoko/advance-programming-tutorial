package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class MultipleFilling {
    public static void main(String[] args) {
        // Thick Bun Burger with Beef Meat, Cheese, Cucumber, Lettuce, and Chili Sauce
        Food thickBunBurgerSpecial = BreadProducer.THICK_BUN.createBreadToBeFilled();
        thickBunBurgerSpecial = FillingDecorator.BEEF_MEAT.addFillingToBread(
        thickBunBurgerSpecial);
        thickBunBurgerSpecial = FillingDecorator.CHEESE.addFillingToBread(
        thickBunBurgerSpecial);
        thickBunBurgerSpecial = FillingDecorator.CUCUMBER.addFillingToBread(
        thickBunBurgerSpecial);
        thickBunBurgerSpecial = FillingDecorator.LETTUCE.addFillingToBread(
        thickBunBurgerSpecial);
        thickBunBurgerSpecial = FillingDecorator.CHILI_SAUCE.addFillingToBread(
        thickBunBurgerSpecial);
        System.out.println(thickBunBurgerSpecial.cost());
        System.out.println(thickBunBurgerSpecial.getDescription());
    }
}
