package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class FrontendProgrammer extends Employees {
    // TODO Implement
    public static final double MIN_SALARY = 30000.00;

    public FrontendProgrammer(String name, double salary) {
        // TODO Implement
        this.name = name;
        this.salary = salary;
        this.role = "Front End Programmer";
        if (salary < MIN_SALARY) {
            throw new IllegalArgumentException("Salary must not below " + MIN_SALARY);
        }
    }

    @Override
    public double getSalary() {
        // TODO Implement
        return this.salary;
    }
}
