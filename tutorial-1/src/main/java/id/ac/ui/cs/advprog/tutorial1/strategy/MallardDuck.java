package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MallardDuck extends Duck {
    // TODO Complete me!
    Duck duck = new MallardDuck();
    private FlyBehavior flyBehavior;
    private QuackBehavior quackBehavior;

    public MallardDuck() {
        flyBehavior = duck.getFlyBehavior();
        quackBehavior = duck.getQuackBehavior();
    }

    public void display() {

    }
}
