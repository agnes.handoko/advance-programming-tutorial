package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck {
    // TODO Complete me!
    // public ModelDuck() {
    // flyBehavior = new FlyNoWay();
    // quackBehavior = new Quack();
    // }
    Duck duck = new ModelDuck();
    private FlyBehavior flyBehavior;
    private QuackBehavior quackBehavior;

    public ModelDuck() {
        flyBehavior = duck.getFlyBehavior();
        quackBehavior = duck.getQuackBehavior();
    }

    public void display() {

    }
}
