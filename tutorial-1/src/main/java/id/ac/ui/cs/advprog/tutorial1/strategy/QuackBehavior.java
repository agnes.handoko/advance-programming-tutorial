package id.ac.ui.cs.advprog.tutorial1.strategy;

public interface QuackBehavior {

    public abstract void quack();
}
